BinaryTelnet
============

This library implements a minimal working telnet protocol that
establishes an 8-bit clean channel and exposes terminal window resize
events.

When used as a client, the following options are requested:

  * Binary Transmission (RFC 856)
  * Suppress Go Ahead (RFC 858)
  * Negotiate About Window Size (RFC 1073)
  * Terminal Type (RFC 1091)
  * Terminal Speed (RFC 1079)
  * New Environment (RFC 1572)

When used as a server, the following options are requested:

  * Binary Transmission (RFC 856)
  * Suppress Go Ahead (RFC 858)
  * Negotiate About Window Size (RFC 1073)
  * Terminal Type (RFC 1091)
  * Terminal Speed (RFC 1079)
  * New Environment (RFC 1572)
  * Echo (RFC 857)



License
-------

This project is licensed under the MIT License.  See the file LICENSE
for the full license text.


Maven
-----

BinaryTelnet is available on Maven Central:

```xml
<dependency>
  <groupId>com.gitlab.klamonte</groupId>
  <artifactId>binarytelnet</artifactId>
  <version>1.0.0</version>
</dependency>
```



Usage
-----

Simple example of connecting as a client:

```Java
import com.klamonte.binarytelnet.*;
import java.io.InputStream;
import java.net.URL;

public class StarWarsViewer {

    public static void main(String [] args) throws Exception {

        // Register telnet as the standard handler for telnet protocol URLs.
        TelnetURLConnection.register();

        // Connect to Star Wars ASCII telnet server.
        URL url = new URL("telnet://towel.blinkenlights.nl");
        InputStream input = url.openStream();

        // Display whatever we get from the server directly to stdout.
        for (int ch = input.read(); ch != -1; ch = input.read()) {
            System.out.printf("%c", (char) ch);
        }
        input.close();
    }

}
```
