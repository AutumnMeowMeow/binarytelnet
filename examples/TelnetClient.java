import com.gitlab.klamonte.binarytelnet.TelnetSocket;

import java.io.InputStream;
import java.io.OutputStream;

public class TelnetClient {

    public static void main(String [] args) throws Exception {
        String host = args[0];
        String port = args[1];
        String term = args[2];
        String user = args[3];
        String lang = args[4];

        TelnetSocket socket = new TelnetSocket(host, Integer.parseInt(port)).terminalType(term).username(user).language(lang);
        InputStream input = socket.getInputStream();
        OutputStream output = socket.getOutputStream();
        while (!socket.isClosed()) {
            if (input.available() > 0) {
                int ch = input.read();
                if (ch == -1) {
                    break;
                }
                System.out.printf("%c", (char) ch);
                System.out.flush();
            }
            if (System.in.available() > 0) {
                int ch = System.in.read();
                if (ch == -1) {
                    break;
                }
                output.write(ch);
                output.flush();
            }
        }
    }

}
