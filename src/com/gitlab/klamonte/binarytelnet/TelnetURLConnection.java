/*
 * BinaryTelnet - Java Telnet Library
 *
 * The MIT License (MIT)
 *
 * Copyright (C) 2019 Kevin Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Kevin Lamonte [kevin.lamonte@gmail.com]
 * @version 1
 */
package com.gitlab.klamonte.binarytelnet;

import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;

/**
 * A URL Connection to a telnet server.
 *
 * <p>
 * The syntax of a telnet connection is:
 */
public class TelnetURLConnection extends URLConnection {

    // ------------------------------------------------------------------------
    // Constants --------------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The socket associated with this connection.
     */
    private TelnetSocket socket;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Constructor for the TelnetURLConnection.
     *
     * @param u - the URL
     */
    protected TelnetURLConnection(URL u) {
        super(u);
        setDoInput(true);
        setDoOutput(true);
    }

    // ------------------------------------------------------------------------
    // URLConnection ----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Register TelnetURLConnection as the class to handle "telnet:" URLs.
     */
    public static void register() {
        URL.setURLStreamHandlerFactory(new TelnetURLStreamHandlerFactory());
    }

    /**
     * Opens a communications link to the resource referenced by this URL, if
     * such a connection has not already been established.
     *
     * If the connect method is called when the connection has already been
     * opened (indicated by the connected field having the value true), the
     * call is ignored.
     *
     * URLConnection objects go through two phases: first they are created,
     * then they are connected. After being created, and before being
     * connected, various options can be specified (e.g., doInput and
     * UseCaches). After connecting, it is an error to try to set
     * them. Operations that depend on being connected, like
     * getContentLength, will implicitly perform the connection, if
     * necessary.
     *
     * @throws SocketTimeoutException if the timeout expires before the
     * connection can be established
     * @throws IOException if an I/O error occurs while opening the
     * connection.
     */
    @Override
    public void connect() throws SocketTimeoutException, IOException {
        assert (getURL().getProtocol().toLowerCase().equals("telnet"));
        int port = getURL().getPort();
        if (port == -1) {
            port = getURL().getDefaultPort();
        }

        socket = new TelnetSocket(getURL().getHost(), port);
    }

    /**
     * Returns an input stream that reads from this open connection. A
     * SocketTimeoutException can be thrown when reading from the returned
     * input stream if the read timeout expires before data is available for
     * read.
     *
     * @return an input stream that reads from this open connection.
     * @throws IOException if an I/O error occurs while creating the input
     * stream.
     */
    @Override
    public InputStream getInputStream() throws IOException {
        if (socket == null) {
            connect();
        }
        return socket.getInputStream();
    }

    /**
     * Returns an output stream that writes to this connection.
     *
     * @return an output stream that writes to this connection.
     * @throws IOException if an I/O error occurs while creating the output
     * stream.
     */
    @Override
    public OutputStream getOutputStream() throws IOException {
        if (socket == null) {
            connect();
        }
        return socket.getOutputStream();
    }

}
